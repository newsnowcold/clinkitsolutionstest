import { GetFleetsResponse } from '../models/get-fleets-response';
import { FleetViewModel } from '../models/fleet-view-model';
import { VehicleType } from '../models/vehicle-type';

export class FleetApiMockData {
  fleet: Array<FleetMockData> = new Array<FleetMockData>();
  res: GetFleetsResponse = { fleets: [] };

  get = (): Array<FleetMockData> => {
    
    this.fleet = [
      {
        Id: 1,
        Name: 'Truck fleet',
        VehicleFleets: [
          {
            VehicleId: 1,
            FleetId: 1,
            DateTime: '',
            Vehicle: {
              Id: 1,
              Name: 'Truck one',
              Type: VehicleType.Truck
            }
          },
          {
            VehicleId: 2,
            FleetId: 1,
            DateTime: '',
            Vehicle: {
              Id: 2,
              Name: 'Truck two',
              Type: VehicleType.Truck
            }
          },
        ]
      },
      {
        Id: 2,
        Name: 'Taxi fleet',
        VehicleFleets: [
          {
            VehicleId: 3,
            FleetId: 2,
            DateTime: '',
            Vehicle: {
              Id: 3,
              Name: 'Taxi one',
              Type: VehicleType.Taxi
            }
          },
          {
            VehicleId: 4,
            FleetId: 2,
            DateTime: '',
            Vehicle: {
              Id: 4,
              Name: 'Taxi two',
              Type: VehicleType.Taxi
            }
          },
        ]
      }
    ]

    return this.fleet;
  }

  getFleet = (): GetFleetsResponse => {
    
    let res: Array<FleetViewModel> = [];

    this.get().map((a) => {
      res.push({
        id: a.Id,
        name: a.Name
      })
    })

    this.res.fleets = res;

    return this.res;
  }
}





class FleetMockData {
	Id: number = 0;
	Name: string = '';
	VehicleFleets: Array<VehicleFleets> = new Array<VehicleFleets>();
}

class VehicleFleets {
  VehicleId: number = 0;	
  FleetId: number = 0;
  DateTime: string = '';
  Vehicle: Vehicle = new Vehicle();
}

class Vehicle {
  Id: number = 0;
  Name: string = '';
  Type: VehicleType = VehicleType.Truck;
}