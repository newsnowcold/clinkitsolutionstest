import { GetVehiclesResponse } from '../../api/models/get-vehicles-response'
import { FleetApiMockData } from './get-fleet-api-response-model';
import { VehicleViewModel } from '../../api/models/vehicle-view-model';

export class VehicleMockData {
  fleet: FleetApiMockData = new FleetApiMockData();
  vehicles: GetVehiclesResponse = { vehicles: [] }

  get = (): GetVehiclesResponse => {
    var res: Array<VehicleViewModel> = [];

    this.fleet.get().map((a) => {
      a.VehicleFleets.map(v => {
        res.push({
          id: v.Vehicle.Id,
          lastKnownLocation: undefined,
          name: v.Vehicle.Name,
          type: v.Vehicle.Type
        })
      })
    })

    this.vehicles.vehicles = res;
    return this.vehicles;
  }

  getVehicle = (vehicleid: any): GetVehiclesResponse => {
    var res: Array<VehicleViewModel> = [];

    this.fleet.get().map((a) => {
      a.VehicleFleets.map(v => {
        if (v.VehicleId == vehicleid) {
          res.push({
            id: v.Vehicle.Id,
            lastKnownLocation: {
              latitude: this.getRandomInRange(-90, 90, 6),
              longitude: this.getRandomInRange(-180, 180, 6),
              timestamp: this.randomDate(new Date(), new Date()).toString()
            },
            name: v.Vehicle.Name,
            type: v.Vehicle.Type
          })
        }
      })
    })

    this.vehicles.vehicles = res;
    return this.vehicles;
  }

  getRandomInRange = (from: any, to: any, fixed: any): number => {
      return (Math.random() * (to - from) + from).toFixed(fixed) * 1;
      // .toFixed() returns string, so ' * 1' is a trick to convert to number
  }

  randomDate = (start: Date, end: Date) => {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime())).getTime();
  }
}

