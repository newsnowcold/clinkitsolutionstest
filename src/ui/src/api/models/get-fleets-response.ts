/* tslint:disable */
/* eslint-disable */
import { FleetViewModel } from './fleet-view-model';
import { FleetApiMockData } from '../mockdata/get-fleet-api-response-model';

export interface GetFleetsResponse {
  fleets?: null | Array<FleetViewModel>;
}
